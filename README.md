# Project 2: Randomized optimization

Matt Hobson  
CS7641 - Spring 2019  
GTID: mhobson7

## Code

Code for this assignment can be found here:

    https://gitlab.com/mhobson/cs7641-assignment2

This was developed under Python 3.7. (You can probably run it with 3.6 or
later.)

If you are familiar with Pipenv, you can use that to set up your virtual
environment. If not, there's a standard requirements file you can use.

The experiments all utilitze the _ABAGAIL_ Java code. As such, you will need to have Jython installed. (Version 2.7.1 was used to develop this.)

## Data sets

The data files are included in the repository. If you prefer to download the
files yourself, there are links to the sourcce UCI pages in the README files
within each data directory.

## Running the project

### Data preparation

Because _ABAGAIL_ does not support cross-validation, the data set must be manually split in advance.

The data in the repository is already split into the needed files, but you can run `python run_experiment.py --dump_data` to regenerate them as needed.

### Run experiments and generate output

Use `jython` to run the following files:
- `NN-Backprop.py`
- `NN-GA.py`
- `NN-RHC.py`
- `NN-SA.py`
- `continuouspeaks.py`
- `flipflop.py`
- `tsp.py`

### Generate plots

Run `python plotting.py` to parse the output files and generate the plots used in the report.

## Acknowledgements

The base of the code used for this project is from:

- [Chad Maron](https://github.com/cmaron/CS-7641-assignments)
- [Jonathan Tay](https://github.com/JonathanTay/CS-7641-assignment-2)
- [_ABAGAIL_ library](https://github.com/pushkar/ABAGAIL)
